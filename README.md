##Teste Duosystem

Desenvolvido em Symfony

====================================================================
Modelo de dados:

https://imgur.com/a/qevYH4m

====================================================================

##Instalando:

Criar uma base de dados chamada 'duosystem' no MySQL e rodar o script de criação das tabelas 'src/scriptTables.sql'.

====================================================================

Alterar o arquivo de configuração /app/parameters.yml para os dados a seguir:

database_host: 127.0.0.1

database_port: 3306

database_name: duosystem

database_user: YOUR_USER

database_password: YOUR_PASSWORD

====================================================================

Na pasta raiz do projeto executar o comando:`$ composer install` 

Subir servidor da aplicação: `$ php bin/console server:start`

Clicar no link gerado no console.

