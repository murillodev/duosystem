create table if not exists `atividade` (
  `id` int(11) not null auto_increment,
  `title` varchar(255) not null,
  `description` varchar(600) not null,
  `start_date` datetime not null,
  `end_date` datetime null,
  `status_id` int(11) not null,
  `situation` tinyint(1) not null default 1,
  primary key (id)
);

create table if not exists `status` (
  `status_id` int(11) not null auto_increment,
  `title` varchar(255) not null,
  primary key (status_id)
);

insert into status (title) values ('Pendente'), ('Em Desenvolvimento'), ('Em Teste'), ('Concluído');
