<?php

namespace Duo\AtividadeBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Duo\ModelBundle\DuoModelBundle;
use Duo\ModelBundle\Entity\Atividade;
use Duo\ModelBundle\Entity\Status;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="atividade_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $id = '';
        $em = $this->getDoctrine()->getManager();
        $lista_status = $em->getRepository('DuoModelBundle:Status')->findAll();

        foreach($lista_status as $key=>$val){
            $status[$val['status_id']] = $val;
        }

        $filtro = ['status_id'=>'','situation'=>''];

        $em = $this->getDoctrine()->getManager();
        $atividade = $em->getRepository('DuoModelBundle:Atividade')->findAll();

        if($request->get('find')){
            $em = $this->getDoctrine()->getManager();
            $status_id = ($request->get('status_id')?$request->get('status_id'):null);
            $situation = ($request->get('situation')?$request->get('situation'):null);
            //exit('status '.$status_id);
            $atividade = $em->getRepository('DuoModelBundle:Atividade')->findFilter(null,$status_id,$situation);
            $filtro = ['status_id'=>$status_id,'situation'=>$situation];
        }

        /** @var  $paginator */
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($atividade, $request->query->get('page', 1), 5);
        return $this->render('atividade/index.html.twig',[
            'pagination' => $pagination,
            'id' => $id,
            'status' => $status,
            'filtro' => $filtro
        ]);
    }

    /**
     * @Route("/cadastro", name="cadastro_index")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function cadastroAction(Request $request)
    {
        $atividade = new Atividade();
        $em = $this->getDoctrine()->getManager();
        $status = $em->getRepository('DuoModelBundle:Status')->findAll();
        $id = '';
		$title = '';
 		$description = '';
 		$start_date = '';
 		$end_date = '';
 		$situation = '';
 		$status_id = '';

        $erro = [];
        $success = [];

        if($request->get('Submit')){   
            $title = $request->get('title');
            $description = $request->get('description');
            $start_date = new \DateTime($request->get('start_date')); 
            $end_date = ($request->get('end_date'))
                            ?new \DateTime($request->get('end_date'))
                            :null; 
            $situation = $request->get('situation'); 
            $status_id = $request->get('status_id');

            try {

                $atividade->setTitle($title);
                $atividade->setDescription($description);
                $atividade->setStartDate($start_date);
                $atividade->setEndDate($end_date);
                $atividade->setSituation($situation);
                $atividade->setStatusId($status_id);

                $em = $this->getDoctrine()->getManager();
                $em->persist($atividade);
                $em->flush($atividade);

                $request->getSession()->getFlashBag()->add('success', 'Atividade criada com sucesso!');

            } catch (Exception $e) {

                $request->getSession()->getFlashBag()->add('error', 'Erro ao criar atividade!');

            }

            return $this->redirect($this->generateUrl('atividade_index', array('id' => $id)));
        }

        return $this->render('atividade/cadastro.html.twig',[
            'error' => $erro,
            'success' => $success,
            'cadastrar' => true,
            'status' => $status,
            'id' => $id,
			'title' => $title,
	 		'description' => $description,
	 		'start_date' => $start_date,
	 		'end_date' => $end_date,
	 		'situation' => $situation,
	 		'status_id' => $status_id
        ]);

    }

    /**
     * @Route("/editar/{id}",name="edit_index")
     * @Template()
     */
    public function editarAction($id, Request $request)
    {
        $atividade   = new Atividade();
        $em          = $this->getDoctrine()->getManager();
        $status      = $em->getRepository('DuoModelBundle:Status')->findAll();
        $atv         = $em->getRepository('DuoModelBundle:Atividade')->findFilter($id);

		$title       = $atv[0]['title'];
 		$description = $atv[0]['description'];
 		$start_date  = $atv[0]['startDate'];
 		$end_date    = $atv[0]['endDate'];
 		$situation   = $atv[0]['situation'];
 		$status_id   = $atv[0]['status_id'];

        $erro        = [];
        $success     = [];

        if($request->get('Submit')){   
            $title = $request->get('title');
            $description = $request->get('description');
            $start_date = new \DateTime($request->get('start_date')); 
            $end_date = new \DateTime($request->get('end_date')); 
            $situation = $request->get('situation'); 

            $status_id = $request->get('status_id');

            try {

                $atividade = $em->getRepository('DuoModelBundle:Atividade')->find($id);

                $atividade->setTitle($title);
                $atividade->setDescription($description);
                $atividade->setStartDate($start_date);
                $atividade->setEndDate($end_date);
                $atividade->setSituation($situation);
                $atividade->setStatusId($status_id);

                $em = $this->getDoctrine()->getManager();

                $em->persist($atividade);

                $em->flush($atividade);

                $request->getSession()->getFlashBag()->add('success', 'Atividade editada com sucesso!');

            } catch (Exception $e) {

                $request->getSession()->getFlashBag()->add('error', 'Erro ao editar atividade!');

            }

            return $this->redirect($this->generateUrl('atividade_index', array('id' => $id)));
        }

        return $this->render('atividade/cadastro.html.twig',[
            'error' => $erro,
            'success' => $success,
            'status' => $status,
            'editar' => (int) $atv[0]['status_id'] !== 4,
            'id' => $id,
			'title' => $title,
	 		'description' => $description,
	 		'start_date' => $start_date,
	 		'end_date' => $end_date,
	 		'situation' => $situation,
	 		'status_id' => $status_id
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_index")
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $lista_status = $em->getRepository('DuoModelBundle:Status')->findAll();

        foreach($lista_status as $key => $val){
            $status[$val['status_id']] = $val;
        }

        try {

            $atividade = $em->getRepository('DuoModelBundle:Atividade')->find($id);

            $em->remove($atividade);

            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Atividade apagada com sucesso!');

        } catch (Exception $e) {

            $request->getSession()->getFlashBag()->add('error', 'Erro ao apagar atividade!');

        }

        return $this->redirect($this->generateUrl('atividade_index'));
    }
}
